from sqlalchemy import Column, ForeignKey,Integer,String, CheckConstraint
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Pacijent(Base):
    __tablename__ = "Pacijenti"
    ID = Column(Integer, primary_key=True) 
    IME = Column(String)
    PREZIME = Column(String)
    OIB = Column(String)    
    ADRESA = Column(String)
    DATUMRODENJA = Column(String)
    SPOL = Column(String)
    BROJOSIGURANEOSOBE = Column(String)
    TELEFON = Column(String)
    EMAIL = Column(String)
    ODABIRDOKTORA = Column(Integer)
    POVIJESTBOLESTI = Column(String)
    DIJAGNOZA = Column(String)
