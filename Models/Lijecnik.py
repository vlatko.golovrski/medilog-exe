from sqlalchemy import Column, ForeignKey,Integer,String, CheckConstraint
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Lijecnik(Base):
    __tablename__ = "Lijecnici"
    ID = Column(Integer, primary_key=True) 
    IME = Column(String)
    PREZIME = Column(String)
    OIB = Column(String)    
    ADRESA = Column(String)
    DATUMRODENJA = Column(String)
    SPOL = Column(String)
    SPECIJALIZACIJA = Column(String)
    REGBROJ = Column(String)   
    TELEFON = Column(String)
    EMAIL = Column(String)
    
